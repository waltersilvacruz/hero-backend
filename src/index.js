const express = require('express');
const approutes = require('./routes/approutes');
const cors = require('cors');

const port = 3333;

const app = express();
app.use(cors());
app.use(express.json());
app.use(approutes);

app.listen(port, () => console.log(`Server started at ${port}`));