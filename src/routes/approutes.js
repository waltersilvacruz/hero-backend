const express = require('express');
const approutes = express.Router();
const ongsRouter = require('./ongs');
const incidentsRouter = require('./incidents');
const profileRouter = require('./profile');
const sessionRouter = require('./sessions');

approutes.use('/ongs', ongsRouter);
approutes.use('/incidents', incidentsRouter);
approutes.use('/profile', profileRouter);
approutes.use('/sessions', sessionRouter);

module.exports = approutes;