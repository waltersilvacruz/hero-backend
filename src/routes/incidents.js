const express = require('express');
const controller = require('../controllers/IncidentController');
const router = express.Router();

router.get('/', controller.index);
router.post('/', controller.create);
router.delete('/:id', controller.delete);

module.exports = router;